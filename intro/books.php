<?php 
class Books {
    protected $result;
    
    function __construct($result){
        $this->result = $result;  
    } 

    public function chart(){
        
        if($this->result->num_rows > 0){
            echo '<table>';
            echo '<tr><th>Titel</th><th>User Name</th></tr>';
            while($row = $this->result->fetch_assoc()){
                echo '<tr>';
                echo '<td>'.$row['titel'].'</td><td>'.$row['name'].'</td>';
                echo '</tr>';
            }
            echo '</table>';
        }else{
            echo "sorry there are no results";
        }
       
    }
} 

?>