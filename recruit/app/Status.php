<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    //ex9
    public function CandidateStatus()
    {
        return $this->hasMany('App\Candidate');
    }

    public static function next($status_id){
       $nexststages = DB::table('nextstages')->where('from',$status_id)->pluck('to');
       return self::find($nexststages)->all();
    }

    public static function allowed($from,$to){
        $allowed=DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
       if(isset($allowed))
           return true;
        else
           return false;
     }
}
