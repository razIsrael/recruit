<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//rout for all my candidates
Route::resource('candidates', 'CandidatesController')->middleware('auth');
// overiding delet route
Route::get('candidades/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');
//sending to my function of choosing user
Route::get('candidades/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
//ex9 adding status
Route::get('candidades/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus');

Route :: get('/hello', function(){
    return 'Hello Laravel';
});

Route :: get('/student/{id}', function($id = 'No student found'){
    return 'Hello Student id Number '.$id;
});

//adding number is optenal because of ?
Route :: get('/car/{id?}', function($id = null){
    if (isset($id)){
        return "This is Car Number $id".'This is Car Number '.$id;
        //'This is Car Number '.$id 
    }else{
        return 'there is No Car Id selected';
    }
    
});

//  Route::get('/comment', function () {
//      return view('comment');
//  });

Route::get('/comment/{id?}', function ($id = 'non selected') {
    return view('comment',compact('id'));
});

// homework ex 5
Route::get('/users/{name?}/{email}', function ($name = 'name missing', $email = null) {
    return view('users',compact('name','email'));
    
});
Route::get('/users/{email}', function ($email = null, $name = 'name missing') {
    return view('users',compact('name','email'));
    
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
