@extends('layouts.app')

@section('title','Edit Candidate')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Candidate</div>
                <div class="card-body">

                    <form action="{{action('CandidatesController@update',$candidate->id)}}" method="post">
                        @method('PATCH')
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Candidate Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name='name' value="{{$candidate->name}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Candidate Email</label>
                            <div class="col-md-6">
                                <input type="text" name='email' class="form-control" value="{{$candidate->email}}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <br>
                                <input type="submit" class="btn btn-primary" name='submit' value='Update Candidate'>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
